# Installation

- Python 3.10
- bleak 0.14.0 (avec pip install bleak=0.14.0)
- asyncio 

# Utilisation

Lancer le script en commande avec comme argument :
- l'addresse MAC de l'objet
- "mac" et l'addresse MAC de l'objet
- "name" et le nom de l'objet (fonctionnalité commentée car non fonctionnelle sur mon appareil)

Ensuite vous êtes guidés par le menu du script

# Fonctionnalités

- lire une caractéristique spécifique ou toutes
- écrire sur une caractéristique spécifique
- s'abonner à une caractéristique spécifique
- lister les fichiers
- télécharger un fichier spécifique ou tous (nécessite d'avoir listé les fichiers)
