import asyncio
from bleak import BleakScanner
from bleak import BleakClient
import sys
from struct import unpack, pack
from collections import namedtuple
from datetime import datetime
from os import mkdir
from os.path import isdir
from binascii import crc32
#name(8) numId(4) numChunck(4)
sizeReceived=0
valReceived=0
LIST=[]
testStream = None
#StructFileInfoGet = unpack("<8sIIQ") FF:F2:A8:91:7F:03
#StructFileInfoSet = pack("<8sIIQ")

FileInfo = namedtuple('FileInfo', ['name', 'size', 'hash', 'date'])
FileReadRequest = namedtuple('FileReadRequest',['name', 'chunkId', 'numChunk'])
file = None
async def callback(uuid, data: bytearray):
    print("f{uuid} : ","-".join(( hex(b)[2:] if (b>15) else '0'+hex(b)[2:]) for b in data))

async def callbackFile(uuid,data: bytearray):
    global sizeReceived,testStream
    sizeReceived += len(data)
    testStream += data
    file.write(data)

async def callbackLIST(uuid, data: bytearray):
    global LIST,valReceived
    print("-".join(( hex(b)[2:] if (b>15) else '0'+hex(b)[2:]) for b in data))
    file_info = FileInfo._make(unpack("<8sIIQ",data))
    print("File ",file_info.name, " de taille ",file_info.size," a comme hash ",hex(file_info.hash)," et a été créé le ",datetime.fromtimestamp(file_info.date/1000))
    idchunk = 0
    numchunk=9999
    file_read_request = pack("<8s4s4s",file_info.name.decode("ascii").encode("utf-8"),idchunk.to_bytes(4,'little'),numchunk.to_bytes(4,'little'))
    print(file_read_request)
    LIST.append((file_read_request,file_info.size,file_info.name.decode("ascii").replace("\x00",""),hex(file_info.hash)))
    valReceived+=1

async def main():
    global file,sizeReceived,valReceived,testStream
    Address = ""
    if (len(sys.argv)==2):
        Address = sys.argv[1]
        devices = await BleakScanner.discover()
        for d in devices:
            if(d.address==Address):
                print("Found : " + Address)
    if (len(sys.argv)==3):
#        if (sys.argv[1]=="name"):
#            name = sys.argv[2]
#            devices = await BleakScanner.discover()
#            for d in devices:
#                if(d.name==name):
#                    Address = str(d).split(" ")[0]
#                    print("Found : " + name + " -> " + Address)
        if (sys.argv[1]=="mac"):
            Address = sys.argv[2]
            devices = await BleakScanner.discover()
            for d in devices:
                #print(d)
                if(d.address==Address):
                    print("Found : " + Address)
    if (Address!=""):
        async with BleakClient(Address) as client:
            is_connected = client.is_connected
            if (is_connected):
                print("Connection to " + Address + " succeded.")
                services = client.services
                print("Services found:")
                for serv in services:
                    print("Service: " + serv.uuid)
                    for char in serv.characteristics:
                        print("\tCharacteristic: " + char.uuid + " ("+ ' & '.join(char.properties) + ")")
                while (is_connected):
                    print("\nOptions:")
                    print("1. Read")
                    print("2. Write")
                    print("3. Subscibe")
                    print("4. Read all")
                    print("5. List Files")
                    print("6. Download")
                    print("7. Download all")
                    print("8. Exit and disconnect")
                    choice = input("Enter your choice: ")
                    if (choice=='1'):
                        uuid = input("Enter characteristic UUID to read from: ")
                        try:
                            val = await client.read_gatt_char(uuid)
                            print("Value: " + "-".join(( hex(b)[2:] if (b>15) else '0'+hex(b)[2:]) for b in val))
                        except Exception as e:
                            print("Failed to read characteristic: " + str(e))
                    elif choice == '2':
                        uuid = input("Enter characteristic UUID to write to: ")
                        val = input("Enter value to write (in hexadecimal 'A20E'): ")
                        try:
                            val_bytes = bytes.fromhex(val)
                            await client.write_gatt_char(uuid, val_bytes, response=True)
                            print("Write successful.")
                        except Exception as e:
                            print("Failed to write to characteristic: " + str(e))
                    elif (choice=='3'):
                        uuid = input("Enter characteristic UUID to subscribe to: ")
                        try:
                            val = await client.start_notify(uuid,callback)
                            print("subscribed to : " + str(uuid))
                        except Exception as e:
                            print("Failed to subscribe: " + str(e))
                    elif choice == '4':
                        for service in services:
                            print("Service: " + str(service.uuid))
                            for char in service.characteristics:
                                if "read" in char.properties:
                                    try:
                                        val = await client.read_gatt_char(char.uuid)
                                        print("\tCharacteristic " + str(char.uuid) + ": " + str(val))
                                    except Exception as e:
                                        print("\tFailed to read characteristic " + str(char.uuid) + ": " + str(e))
                                else:
                                    print("\tCharacteristic " + str(char.uuid) + " does not support reading.")
                    elif (choice=='5'):
                        valReceived=0
                        try:
                            await client.start_notify("1b0d1302-a720-f7e9-46b6-31b601c4fca1",callbackLIST)
                        except Exception as e:
                            print("Failed to subscribe: " + str(e))
                        try:
                            val = await client.read_gatt_char("1b0d1303-a720-f7e9-46b6-31b601c4fca1")
                        except Exception as e:
                            print("Failed to read characteristic: " + str(e))
                        try:
                            await client.write_gatt_char("1b0d1302-a720-f7e9-46b6-31b601c4fca1", val, response=True)
                        except Exception as e:
                            print("Failed to write to characteristic: " + str(e))
                        while(valReceived < int.from_bytes(val,'little')):
                            #print(valReceived, "/",int.from_bytes(val,'little'))
                            await asyncio.sleep(0.1)
                        await client.stop_notify("1b0d1302-a720-f7e9-46b6-31b601c4fca1")
                    elif (choice=='6'):
                        if LIST==[]:
                            print("vous n'avez pas fait : 5. List Files")
                        else:
                            name = input("Enter the name of the file :")
                            try:
                                await client.start_notify("1b0d1304-a720-f7e9-46b6-31b601c4fca1",callbackFile)
                            except Exception as e:
                                print("Failed to subscribe: " + str(e))
                            if (not isdir("file")):
                                mkdir("file")
                            for l in range(len(LIST)):
                                sizeReceived=0
                                testStream = bytearray()
                                if (LIST[l][2]==name):
                                    file = open("file\\"+LIST[l][2],"wb")
                                    try:
                                        await client.write_gatt_char("1b0d1304-a720-f7e9-46b6-31b601c4fca1", LIST[l][0], response=True)
                                    except Exception as e:
                                        print("Failed to write to characteristic: " + str(e))
                                    while(sizeReceived < LIST[l][1]):
                                        await asyncio.sleep(0.1)
                                    file.close()
                                    print(LIST[l][2]+" : ")
                                    print("\tcrc32 : ",hex(crc32(testStream)),"|",LIST[l][3]," : hash")
                                    print("\treçu : ",len(testStream)," / ",LIST[l][1])
                            await client.stop_notify("1b0d1304-a720-f7e9-46b6-31b601c4fca1")
                    elif (choice=='7'):
                        if LIST==[]:
                            print("vous n'avez pas fait : 5. List Files")
                        else:
                            try:
                                await client.start_notify("1b0d1304-a720-f7e9-46b6-31b601c4fca1",callbackFile)
                            except Exception as e:
                                print("Failed to subscribe: " + str(e))
                            if (not isdir("file")):
                                mkdir("file")
                            for l in range(len(LIST)):
                                sizeReceived=0
                                testStream = bytearray()
                                file = open("file\\"+LIST[l][2],"wb")
                                try:
                                    await client.write_gatt_char("1b0d1304-a720-f7e9-46b6-31b601c4fca1", LIST[l][0], response=True)
                                except Exception as e:
                                    print("Failed to write to characteristic: " + str(e))
                                while(sizeReceived < LIST[l][1]):
                                    await asyncio.sleep(0.1)
                                file.close()
                                print(LIST[l][2]+" : ")
                                print("\tcrc32 : ",hex(crc32(testStream)),"|",LIST[l][3]," : hash")
                                print("\treçu : ",len(testStream)," / ",LIST[l][1])
                            await client.stop_notify("1b0d1304-a720-f7e9-46b6-31b601c4fca1")
                    elif choice == '8':
                        print("Disconnecting...")
                        is_connected = False
                    else:
                        print("Invalid choice.")
            else:
                print("Failed to connect.")

        print("Disconnected.")
    else:
        print("Not Found.")

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
